<?php

namespace Dojo\Tests;

use Dojo\Unoconvert;
use PHPUnit\Framework\TestCase;

class UnoconvertTest extends TestCase
{
    public function testExecute():void
    {
        $unoconvert = new Unoconvert(__DIR__."/convert-test.sh");
        $filename = __DIR__.'/Bienvenue.odt';
        $response = $unoconvert->execute($filename);
        $this->assertStringStartsWith('%PDF', $response);
    }

    public function testCommandeIntrouvable():void
    {
        $this->expectException(\Exception::class);
        new Unoconvert(__DIR__."/blabla.sh");
    }

    public function testExecuteFichierIntrouvable():void
    {
        $this->expectException(\Exception::class);
        $unoconvert = new Unoconvert(__DIR__."/convert-test.sh");
        $filename = __DIR__.'/Bienvenue4.odt';
        $response = $unoconvert->execute($filename);
    }

    public function testExecuteConversionRatee():void
    {
        $this->expectException(\Exception::class);
        $unoconvert = new Unoconvert(__DIR__."/convert-test.sh");
        $filename = 'tests/Bienvenue2.odt';
        $response = $unoconvert->execute($filename);
    }
}
