<?php

namespace Dojo\Tests;

use Dojo\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    public function testGetUploadFilename():void
    {
        $filename = __DIR__."/Bienvenue.odt";
        $FAKE_FILE = [
            'file' => [
                'tmp_name' => $filename
                ]
            ];
        $request = new Request($FAKE_FILE);
        $f = $request->getUploadFilename();
        $this->assertSame($filename, $f);
    }

    public function testGetUploadFilenameExceptionIllisible():void
    {
        $this->expectException(\Exception::class);
        $filename = __DIR__."/Bienvenue.odt";
        $FAKE_FILE = [
            'file' => [
                'tmp_name2' => $filename
                ]
            ];
        $request = new Request($FAKE_FILE);
        $f = $request->getUploadFilename();
    }

    public function testConstruct():void
    {
        $filename = __DIR__."/Bienvenue.odt";
        $_FILES = [
            'file' => [
                'tmp_name' => $filename
                ]
            ];
        $request = new Request();
        $f = $request->getUploadFilename();
        $this->assertSame($filename, $f);
    }
}
