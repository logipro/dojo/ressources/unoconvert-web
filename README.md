# unoconvert-web

Une interface HTTP en PHP permettant d'accèder à la commande unoconvert

# Installation

```bash
git clone git@gitlab.com:logipro/dojo/ressources/unoconvert-web.git
cd unoconvert-web
bin/composer install
```
## Environnement de prod
En pré-requis, le serveur sur lequel est installé
1. dispose de unoconvert et unoserver + http + php
2. le routage http doit être:
* https://[serveurDeProd]/[votreChoix] qui route le dossier src/Infrastructure/Web
* les fichiers index.php doivent être configurés pour s'éexcuter par défaut au niveau du serveur http.

Pour fonctionner le script attend une requete contenant un fichier. La réponse http sera le fichier convertit en http.

## Environnement de développement
pour disposer de la commande `bin/unoconvert [fichierAConvertirEnPdf]`
```bash
docker build -t unoconvert docker
```

# Lancement

Les tests unitaires
```bash
bin/phpunit
```

Vérifications du typage, de la syntaxe, du passage des tests unitaires.

```bash
./codecheck
```

# Autre chose ?

Appel direct au langage, par exemple la version utilisée
```bash
bin/php --version
```

Appel à divers outils de vérification de la qualité
```bash
bin/qa
```
