<?php

namespace Dojo;

class Unoconvert
{
    const COMMAND_PROD = 'unoconvert --convert-to PDF --port 2020';

    public function __construct(
        private string $scriptConvert = self::COMMAND_PROD
    ) {
        if (`which $this->scriptConvert` == false) {
            throw new \Exception("Commande introuvable:".$this->scriptConvert);
        }
    }

    public function execute(string $filename):string
    {
        $this->checkFilename($filename);
        $commande = $this->buildCommand($filename);
        $output = $this->tryExec($commande);

        return $output;
    }

    private function checkFilename(string $filename):void
    {
        if (!file_exists($filename)) {
            throw new \Exception("Le fichier n'existe par! $filename");
        }
    }

    private function buildCommand(string $filename):string
    {
        return $this->scriptConvert." ".$filename." -";
    }

    private function tryExec(string $commande):string
    {
        $output = shell_exec($commande);
        if ($output == null || $output == false) {
            throw new \Exception("La commande $commande a été lancée mais n'a rien renvoyé");
        }
        return $output;
    }
}
