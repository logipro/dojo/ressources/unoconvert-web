<?php

namespace Dojo;

class Request
{
    /** @var array<string,array<string,string>> $files */
    private array $files;

    /**
     * @param ?array<string,array<string,string>> $files
     */
    public function __construct(?array $files = null)
    {
        if ($files === null) {
            $this->files = $_FILES;
        } else {
            $this->files = $files;
        }
    }

    public function getUploadFilename():string
    {
        $this->checkIfFileExists();
        $document =  $this->files['file']['tmp_name'];
        return $document;
    }

    private function checkIfFileExists():void
    {
        if (!isset($this->files['file']['tmp_name'])) {
            throw new \Exception("Je ne trouve pas de document uploadé");
        }
    }
}
