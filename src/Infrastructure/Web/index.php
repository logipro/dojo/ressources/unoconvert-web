<?php

namespace Dojo\Infrastructure\Web;

use Dojo\Request;
use Dojo\Unoconvert;

include_once(__DIR__."/../../../vendor/autoload.php");

$request = new Request();
$document = $request->getUploadFilename();

$unoconvert = new Unoconvert();
echo $unoconvert->execute($document);
